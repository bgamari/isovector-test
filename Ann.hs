{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
module Ann where

import Language.Haskell.TH.Syntax as TH
import Data.Data
import Data.Binary
import GHC.Generics

data Annotation = Annotation [TH.Name]
  deriving (Data, Generic)

