{-# LANGUAGE CPP #-}
module Plugin where

#if MIN_VERSION_ghc(9,0,0)
import GHC.Tc.Utils.Monad
import GHC.ThToHs (convertToHsExpr)
import GHC.Types.Basic (Origin(..))
import GHC.Utils.Error (MsgDoc)
#elif MIN_VERSION_ghc(8,10,0)
import TcRnMonad as GHC.Tc.Utils.Monad
import GHC.ThToHs (convertToHsExpr)
import RnEnv (lookupOccRn)
import Plugins (Plugin)
import BasicTypes (Origin(..))
import ErrUtils (MsgDoc)
import TcEnv (tcLookupId)
import Annotations
#endif

import GHC
import Language.Haskell.TH.Syntax as TH

resolveTHName :: TypecheckedModule -> TH.Name -> RealSrcSpan -> Ghc (Maybe Id)
resolveTHName tcm th_name src_span = do
  hsc_env <- getSession
  let (gbl_env, _) = tm_internals_ tcm
  (msgs, mb_id) <- liftIO $ GHC.Tc.Utils.Monad.initTcWithGbl hsc_env gbl_env src_span $ do
    Right rdr <- pure $ cvtName FromSource undefined th_name
    name <- lookupOccRn (unLoc rdr)
    tcLookupId (unLoc name)
  return mb_id

cvtName :: Origin -> SrcSpan -> TH.Name -> Either MsgDoc (Located GHC.RdrName)
cvtName origin span v =
  fmap (f . unLoc) (convertToHsExpr origin span (VarE v))
    where
      f (HsVar _ v') = v'
      f _ = error "this shouldn't happen"

plugin :: Plugin
plugin = defaultPlugin { installCoreToDos = \opts todos -> coreTodo : todos }
  where
    coreTodo = CoreDoPluginPass "test" pass

    pass :: CorePluginPass
    pass guts = do
      findAnns (mg_anns guts
